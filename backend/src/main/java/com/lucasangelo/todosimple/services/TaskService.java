package com.lucasangelo.todosimple.services;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.lucasangelo.todosimple.repositories.TaskRepository;

@Service
public class TaskService {

    private final ModelMapper mapper;
    private final TaskRepository taskRepository;

    public TaskService(ModelMapper mapper, TaskRepository taskRepository) {
        this.mapper = mapper;
        this.taskRepository = taskRepository;
    }

}
