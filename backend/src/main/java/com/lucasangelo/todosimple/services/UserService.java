package com.lucasangelo.todosimple.services;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.lucasangelo.todosimple.repositories.UserRepository;

@Service
public class UserService {
    
    private final ModelMapper mapper;
    private final UserRepository userRepository;


    public UserService(ModelMapper mapper, UserRepository userRepository) {
        this.mapper = mapper;
        this.userRepository = userRepository;
    }

}
