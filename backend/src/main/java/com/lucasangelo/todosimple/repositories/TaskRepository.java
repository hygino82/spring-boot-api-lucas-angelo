package com.lucasangelo.todosimple.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lucasangelo.todosimple.models.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    // @Query(value = "SELECT obj FROM Task obj WHERE obj.user.id = :id")
    // public List<TaskRepository> findByUserId(@Param("id") Long id);

    public List<TaskRepository> findByUser_Id(Long id);

    // @Query(nativeQuery = true, value = "SELECT * FROM tb_task obj WHERE
    // obj.user_id = :id")
    // public List<TaskRepository> findByUserIdSql(@Param("id") Long id);
}
