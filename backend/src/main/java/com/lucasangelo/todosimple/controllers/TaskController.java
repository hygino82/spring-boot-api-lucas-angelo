package com.lucasangelo.todosimple.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lucasangelo.todosimple.services.TaskService;

@RestController
@RequestMapping("api/v1/task")
public class TaskController {

    private final TaskService TaskService;

    public TaskController(TaskService TaskService) {
        this.TaskService = TaskService;
    }

}
